<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=jsonconsole',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8mb4',
            'enableQueryCache'=>true,
        ],
         'cache' => [
            'class' => 'yii\caching\DbCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
//	    'useFileTransport' => false,
//            'transport' => [
//            'class' => 'Swift_SmtpTransport',
//            'host' => 'smtp.provider.com',  
//            'username' => 'noreplymap@demomap.dev',
//            'password' => 'root',
//            'port' => '587', 
//            'encryption' => 'tls', 
//            ],
        ],
    ],
];
