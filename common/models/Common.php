<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\HtmlPurifier;


class Common extends \yii\db\ActiveRecord {
    private $commonStringFields;
        
    public function __construct($config = array()) {
        parent::__construct($config);
        
        $this->commonStringFields = array('text','hastags','tweet_account','tweet_id','name','address','url','description','phone','logo','place','descr','twitter_url','fb_url','linkedin_url','event_date','date','email');
        
    }
    
    /**
     * Sanitizes data coming from data.json
     * @param type $array
     */
    public function PrepDataW($array){
        $values = array();
        
        foreach ($array as $w=>$value){
            foreach($value as $k=>$v){
                if(!is_int($v)){
                    $values[$w][$k] = htmlentities(htmlspecialchars(strip_tags($v)));
                }
                else{
                    $values[$w][$k] = $v;
                }
            }
        }
        
        return $values;
    }
    
    
        public function writeFile($filename, $err_txt){
        $err= date('D, d M Y H:i:s'). PHP_EOL;
        $err .="". PHP_EOL;
        $err .="". PHP_EOL;
        $err .=$err_txt. PHP_EOL;
        $err .="". PHP_EOL;
        $err .="--------------------------------------------------------------------------------------". PHP_EOL;
        $err .="". PHP_EOL;
        
                
        // Write the contents to the file, 
        // using the FILE_APPEND flag to append the content to the end of the file
        // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
        file_put_contents($filename, $err, FILE_APPEND | LOCK_EX);
    }
}
