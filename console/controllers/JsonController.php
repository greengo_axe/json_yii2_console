<?php
namespace console\controllers;

use \Yii;
use yii\console\Controller;
use common\models\Common;

class JsonController extends Controller {
   
    public function actionSavetodb()
    {
        $modelCommon = new Common();
        $filepath = '/var/www/html/yiiconsole/json_yii2_console/console';
        $file_err_save = $filepath.'/logs/err_save.txt';
        $file_json = $filepath.'/data/data.json';
	$json  = '';
	$array = '';
        
        try{//checks if json file exists
        $json = file_get_contents($file_json);
	$array = json_decode($json, true);
            if(is_array($array) && !empty($array)){// it runs only if the array is not empty
                $data = $modelCommon->PrepDataW($array['data']);
                
                foreach($data as $k=>$v){
                    $qr = Yii::$app->db->createCommand()->insert('json', $v)->execute();
                    
                    if(!$qr){
                        echo 'Some errors occurred with db'. PHP_EOL;
                        $modelCommon->writeFile($file_err_save,'Some errors occurred with db');   
//                        break;
                    }
                }
            }// end if "array not empty"
            else{//writes error logs
                echo 'Some errors occurred with JSON file'. PHP_EOL;
                $modelCommon->writeFile($file_err_save, 'No JSON data');
               
            }
        }// end try "no json file"
        catch (\yii\base\ErrorException $e){// writes error logs
            echo 'Some errors occurred'. PHP_EOL;
            $modelCommon->writeFile($file_err_save, $e->getMessage());
            
        }
    }
}